<h1 align="center">Hi there, I'm Arnest</h1>
<h3 align="center">A passionate backend developer from France</h3>

---

### My portfolio website

https://portfolio.arnestcruze.com

### GitHub Stats

![Most used languages](https://github-readme-stats.vercel.app/api/top-langs/?username=ocruze&langs_count=10&layout=compact&hide=tsql)

![Github Stats](https://github-readme-stats.vercel.app/api?username=ocruze&count_private=true&show_icons=true&title_color=fff&text_color=fff&bg_color=30,36d1dc,904e95)

![Streak](https://github-readme-streak-stats.herokuapp.com/?user=ocruze&)

![Trophies](https://github-profile-trophy.vercel.app/?username=ocruze)

### Connect with me

<p align="left">
  <a href="mailto:o.cruze@live.com" target="blank"><img align="center" src="https://upload.wikimedia.org/wikipedia/commons/d/df/Microsoft_Office_Outlook_%282018%E2%80%93present%29.svg" alt="o-a-cruze" height="30" width="40" /></a>
  <a href="https://linkedin.com/in/o-a-cruze" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="o-a-cruze" height="30" width="40" /></a>
</p>

### Recent activity

<!--RECENT_ACTIVITY:start-->
1. ⭐ Starred [symfony/symfony](https://github.com/symfony/symfony)
2. 🔱 Forked [ocruze/geostyler-qgis-parser](https://github.com/ocruze/geostyler-qgis-parser) from [geostyler/geostyler-qgis-parser](https://github.com/geostyler/geostyler-qgis-parser)
3. 💪 Opened PR [#91](https://github.com/IGNF/geotuileur-site/pull/91) in [IGNF/geotuileur-site](https://github.com/IGNF/geotuileur-site)
4. 🎉 Merged PR [#1](https://github.com/ocruze/webpack-demo/pull/1) in [ocruze/webpack-demo](https://github.com/ocruze/webpack-demo)
5. 🔱 Forked [ocruze/Grafikart.fr](https://github.com/ocruze/Grafikart.fr) from [Grafikart/Grafikart.fr](https://github.com/Grafikart/Grafikart.fr)
<!--RECENT_ACTIVITY:end-->

<!--RECENT_ACTIVITY:last_update-->
Last Updated: Thursday, August 25th, 2022, 2:20:44 AM
<!--RECENT_ACTIVITY:last_update_end-->

### Profile visit count

![Profile visit count](https://profile-counter.glitch.me/ocruze/count.svg)
